// console.log('Hello, World');

// Array Traversals
// an array in programming is simply a list of data. Data that are related/connected with each other.

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

// Now, with an array, we can simply write the code above like this:
let studentNumbers = [2020, '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

console.log(studentNumbers);

studentNumbers.length = studentNumbers.length - 1;
console.log(studentNumbers);

// Arrarys are used to store multiple related values in a single variable
// Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
// Arrays also provide access to a number of functions/methods that help in achieving specific task
// A method is another term for functions associated with an object/array and is used to execute astatements that are relevant
// Majority of methods are used to manipulate information stored within the same object
// Arrays are also object which is another type

// To delete a specific item in array we can employ array methods or an algorithm

studentNumbers.length--;
console.log(studentNumbers);

// but we cant do the same with strings
fullName = 'John Doe';
fullName.length--;
console.log(fullName);
console.log(fullName.length);

// You can also lenghten an array by adding a number into the length property
let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length += 2;

console.log(theBeatles);

theBeatles[4] = 'Qiu';
console.log(theBeatles);

// Store/Reassign array elements in another variable
let lakers = ['Kobe', 'Shaq', 'Lebron', 'Magic',' Kareem'];
let currentLaker = lakers[2];
console.log(lakers);

lakers[4] = 'Pacquiao';
console.log(currentLaker);
console.log(lakers);

//

let newArr = [];
newArr[2] = 'Cloud Strife';
console.log(newArr);

// Multidimensional Arrays
let chessBoard = [
        ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
        ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
        ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
        ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
        ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
        ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
        ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
        ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
        ];

console.table(chessBoard);